import org.junit.Assert;
import org.junit.Test;


public class PayrollTest {

    @Test
    public void shouldAddPossibleSalary0() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "0";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1a() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1a";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1b() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1b";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1c() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1c";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1d() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1d";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1e() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1e";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary1ocen() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "1ocen";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2a() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2a";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2b() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2b";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2c() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2c";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2d() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2d";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2e() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2e";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary2ocen() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "2ocen";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary3a() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "3a";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary3b() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "3b";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary3ocen() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "3ocen";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary4a() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "4a";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary4b() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "4b";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary4ocen() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "4ocen";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary5a() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "5a";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary5b() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "5b";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary5ocen() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "5ocen";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary6() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "6";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary7() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "7";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary8() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "8";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary10() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "10";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary11() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "11";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary12() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "12";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary13() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "13";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary14() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "14";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary15() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "15";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

    @Test
    public void shouldAddPossibleSalary16() throws Exception {
        Payroll payroll = new Payroll();
        String pathname  = "16";
        payroll.check(pathname);
        for(int i =1; i<=payroll.getNumberOfWorkers();i++) {
            Assert.assertEquals(payroll.getWorkerSalary(i), payroll.getOutputSalaryList()[i-1]);
        }
    }

}