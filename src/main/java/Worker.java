import java.util.ArrayList;
import java.util.List;

public class Worker {
    private int idNumber;
    private int salary;
    private List<Worker> subordinates;
    private int numberOfAllSubordinates;
    private Worker boss;

    public Worker() {
        idNumber = 0;
        salary = 0;
        numberOfAllSubordinates = 0;
        subordinates = new ArrayList<Worker>();
    }


    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void addSubordinates(Worker worker) {
        subordinates.add(worker);
    }

    public void addBoss(Worker boss) {
        this.boss = boss;
    }

    public List<Worker> getSubordinates() {
        return subordinates;
    }

    public int getNumberAllSubordinateWithoutSalary() {
        int counter =0;
        for(int i = 0; i<subordinates.size();i++) {
            if(subordinates.get(i).getSalary()==0) {
                counter +=subordinates.get(i).getNumberOfAllSubordinates()+1;
            }
        }
        return counter;
    }

    public Worker getSubordinateWithoutSalary() {
        for(int i = 0; i<subordinates.size();i++) {
            if(subordinates.get(i).getSalary()==0) {
                return subordinates.get(i);
            }
        }
        return null;
    }

    public int getSalary() {
        return salary;
    }

    public int getNumberOfDirectSubordinates() {
        return subordinates.size();
    }

    public void addToNumberOfAllSubordinates(List<Worker> subordinates) {
        this.numberOfAllSubordinates = getNumberOfDirectSubordinates();
                for(int i=0; i<subordinates.size();i++) {
                    this.numberOfAllSubordinates +=subordinates.get(i).getNumberOfAllSubordinates();
                }
    }

    public int getNumberOfAllSubordinates() {
        return numberOfAllSubordinates;
    }

    public int getNumberDirectSubordinatesWitoutSalary() {
        int counter= 0;
        for(int i = 0; i<subordinates.size();i++){
            if(subordinates.get(i).getSalary()==0) counter++;
        }
        return counter;
    }


}
