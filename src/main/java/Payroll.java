import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Payroll {

    private int[] superiorsList;
    private int[] inputSalaryList;
    private int[] usedSalary;
    private int numberOfWorkers;
    private List<Worker> workers;
    private Worker mainBoss;
    private int[] outputSalaryList;

    public void check(String numberInput) {
        readInputFromFile(numberInput);
        readOutputFromFile(numberInput);
        addWorkers();
        calculateNumberAllSubordinates();
        findSalary();
        writeToFile(numberInput);
    }


    public void readInputFromFile(String pathname) {
        File file = new File("pen" + pathname + ".in");
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }

        numberOfWorkers = Integer.parseInt(scanner.nextLine());
        workers = new ArrayList<Worker>(numberOfWorkers);
        superiorsList = new int[numberOfWorkers];
        inputSalaryList = new int[numberOfWorkers];
        for (int i = 0; i < numberOfWorkers; i++) {
            superiorsList[i] = scanner.nextInt();
            inputSalaryList[i] = scanner.nextInt();
        }
    }

    public void readOutputFromFile(String pathname) {
        File file = new File("pen" + pathname + ".out");
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        outputSalaryList = new int[numberOfWorkers];
        for (int i = 0; i < numberOfWorkers; i++) {
            outputSalaryList[i] = scanner.nextInt();
        }
    }

    public void writeToFile(String pathname) {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter("pen" + pathname + "c.out");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        for (int i = 0; i < numberOfWorkers; i++) {
            printWriter.println(workers.get(i).getSalary());
        }

        printWriter.close();
    }

    public void addWorkers() {
        for (int i = 0; i < numberOfWorkers; i++) {
            workers.add(new Worker());
            workers.get(i).setIdNumber(i + 1);
            workers.get(i).setSalary(inputSalaryList[i]);
            if (i + 1 == superiorsList[i]) {
                mainBoss = workers.get(i);
            }
        }

        for (int i = 0; i < numberOfWorkers; i++) {
            Worker worker = workers.get(i);
            if (worker.getIdNumber() != superiorsList[i]) {
                Worker boss = workers.get(superiorsList[i] - 1);
                worker.addBoss(boss);
                boss.addSubordinates(workers.get(i));
            }
        }
    }

    public void findSalary() {
        usedSalary = new int[numberOfWorkers];
        traversePreOrder(mainBoss);

        int j = 0;
        int possibleSalary = 0;
        int freeSalaryCounter = 0;

        if (numberOfWorkers == 1) {
            workers.get(0).setSalary(1);
        }
        while (j < numberOfWorkers - 1) {
            while (usedSalary[j] == 0) {
                freeSalaryCounter++;
                possibleSalary++;
                j++;
            }
            int salary = j;
            j++;
            Worker boss = workers.get(usedSalary[salary] - 1);
            freeSalaryCounter -= boss.getNumberAllSubordinateWithoutSalary();
            if (freeSalaryCounter == 0) {
                possibleSalary = setSalary(possibleSalary, salary, boss);
            } else {
                if (boss.getNumberAllSubordinateWithoutSalary() != 0) {
                    possibleSalary = 0;
                }
            }
        }
    }

    private int setSalary(int possibleSalary, int salary, Worker boss) {
        while (possibleSalary != 0 && boss.getNumberDirectSubordinatesWitoutSalary() == 1) {
            Worker nextBoss = boss.getSubordinateWithoutSalary();
            while (usedSalary[salary - 1] != 0) salary--;
            usedSalary[salary - 1] = boss.getSubordinateWithoutSalary().getIdNumber();
            boss.getSubordinateWithoutSalary().setSalary(salary);
            boss = nextBoss;
            possibleSalary--;
        }
        return possibleSalary;
    }

    public void traversePreOrder(Worker worker) {
        if (worker == null) return;

        Stack<Worker> stack = new Stack<Worker>();
        stack.push(worker);

        while (!stack.isEmpty()) {
            Worker temp = stack.peek();
            if (temp.getSalary() != 0) {
                usedSalary[temp.getSalary() - 1] = temp.getIdNumber();
            }
            stack.pop();

            for (int i = 0; i < temp.getNumberOfDirectSubordinates(); i++) {
                stack.push(temp.getSubordinates().get(i));
            }
        }
    }

    public void calculateNumberAllSubordinates() {
        traversePostOrder(mainBoss);
    }

    public void traversePostOrder(Worker worker) {

        Stack<Worker> stack1 = new Stack<Worker>();
        Stack<Worker> stack2 = new Stack<Worker>();

        if (worker == null) return;
        stack1.push(worker);

        while (!stack1.isEmpty()) {
            Worker temp = stack1.pop();
            stack2.push(temp);

            for (int i = 0; i < temp.getNumberOfDirectSubordinates(); i++) {
                stack1.push(temp.getSubordinates().get(i));
            }
        }

        while (!stack2.isEmpty()) {
            Worker temp = stack2.pop();
            if (temp.getNumberOfDirectSubordinates() != 0) {
                temp.addToNumberOfAllSubordinates(temp.getSubordinates());
            }
        }
    }

    public int getNumberOfWorkers() {
        return numberOfWorkers;
    }

    public int[] getOutputSalaryList() {
        return outputSalaryList;
    }

    public List<Worker> getWorkers() {
        return workers;
    }
    private Worker getOneWorker(int idNumber) {
        return workers.get(idNumber-1);
    }

    public int getWorkerSalary(int idNumber) {
        return getOneWorker(idNumber).getSalary();
    }
}
